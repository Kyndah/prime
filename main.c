#include <stdio.h>
#include "prime.h"

int main(int argc, char *argv[]){
    if(argc!=3){
       return 1;
    }
    int min = atoi(argv[1]);
    int max = atoi(argv[2]);

    prime(min, max);
    return 0;

}


