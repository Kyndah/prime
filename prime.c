#include <stdio.h>
#include "prime.h"

void prime(int min, int max){
    if(min<0){
        printf("The given minimum value is a negative number, so I take its absolute value.\n");
        min=min*(-1);
    }
    if(max<0){
        printf("The given maximum value is a negative number, so I take its absolute value.\n");
        max=max*(-1);
    }
    if(min>max){
        printf("The given minimum value is bigger than the maximum, so I swap them.\n");
        int *p1, *p2, seged;
            p1 = &min;
            p2 = &max;
            seged = *p1;
            *p1 = *p2;
            *p2 = seged;
    }
        int oszt, i, marad;
        for(i=min; i<=max; i++){
                if(i<2){
                    continue;
                }
                marad=1;
                int s=sqrt(i);
                for(oszt=2; oszt<=s && marad==1; oszt++){
                        if(i%oszt==0){
                                marad=0;
                        }
                }
                if(marad==1){
                        printf("%d\n", i);
                }
        }

}



